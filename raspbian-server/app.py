import os
import threading
import requests as r
from flask import Flask, request
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)

RESULTS_SERVER_URL = os.getenv('RESULTS_SERVER')
print('server: ' , RESULTS_SERVER_URL)

@app.route('/api/accelerometer', methods=['POST'])
def accelerometer_input():
	print('Sending accelerometer data to Results Server...', request.data)
	try:
		send_sensor_data('accelerometer', request.data)
	except Exception as e:
		print('ERROR', e)
		return { 'success': False }
	return { 'success': True }

@app.route('/api/microphone', methods=['POST'])
def microphone_input():
	print('Sending microphone data to Results Server...', request.data)
	try:
		send_sensor_data('microphone', request.data)
	except Exception as e:
		print('ERROR', e)
		return { 'success': False }
	return { 'success': True }

@app.route('/api/camera', methods=['POST'])
def camera_input():
	print('Sending camera data to Results Server...', request.data)
	try:
		send_sensor_data('camera', request.data)
	except Exception as e:
		print('ERROR', e)
		return { 'success': False }
	return { 'success': True }

def send_sensor_data(sensor, data):
        name = "{url}/api/{sensor}".format(url = RESULTS_SERVER_URL , sensor=sensor)
        res = r.post(name, json=data)

def run_app(x):
	app.run(port=5001)

app_thread = threading.Thread(target=run_app, args=('aux',))

app_thread.start()
