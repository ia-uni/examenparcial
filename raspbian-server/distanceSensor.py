import app


from EmulatorGUI import GPIO
import time
import traceback
import requests

class DistanceSensor:
    
    def setUp(self , GPIO_TRIGGER , GPIO_ECHO, speed):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        self.GPIO_TRIGGER = GPIO_TRIGGER
        self.GPIO_ECHO = GPIO_ECHO
        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)
        self.calibration = speed
        
    def readDistance(self):
        GPIO.output(self.GPIO_TRIGGER , True)

        time.sleep(0.00001)
        GPIO.output(self.GPIO_TRIGGER, False)
        StartTime = time.time()
        StopTime = time.time()
        while GPIO.input(self.GPIO_ECHO) == 0:
            StartTime = time.time()

        while GPIO.input(self.GPIO_ECHO) == 1:
            StopTime = time.time()

        TimeElapsed = StopTime - StartTime

        distance = (TimeElapsed * self.calibration) / 2
        return distance
		
    def sendData(self, data):
        data = "{:.2f}".format(data)
        app.send_sensor_data('distance' , {'value' : data })
        print("distance = " , data , " cm")
        


if __name__ == '__main__':
    sensor = DistanceSensor()
    sensor.setUp(18, 24, 300)
    try:
        while(True):
            sensor.sendData(sensor.readDistance())
            time.sleep(0.1)
    except Exception as ex:
        traceback.print_exc()
    finally:
        GPIO.cleanup() #this ensures a clean exit



