import app

from EmulatorGUI import GPIO
import time
import traceback
import requests

class DHT:
    def setup(self , pin1, pin2, delta):
        self.pin1 = pin1
        self.pin2 = pin2
        self.humidity = 85
        self.delta = delta
        
    def read(self):
        while GPIO.input(self.pin1) == 1:
            self.humidity -= self.delta
            time.sleep(0.1)

        while GPIO.input(self.pin2) == 1:
            self.humidity += self.delta
            time.sleep(0.1)

        return self.humidity
        

class HumiditySensor:
    
    def setUp(self , pin1, pin2, calibration):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.pin1 = pin1
        self.pin2 = pin2
        self.sensor = DHT()
        self.sensor.setup(pin1 , pin2 , calibration)
        self.calibration = calibration
        GPIO.setup(self.pin1, GPIO.IN)
        GPIO.setup(self.pin2, GPIO.IN)
        
        
        
    def readHumidity(self):
        return self.sensor.read()
		
    def sendData(self, data):
        data = "{:.2f}".format(data)
        app.send_sensor_data('humidity' , {'value' : data})
        print("humidity = " , data)
        


if __name__ == '__main__':
    sensor = HumiditySensor()
    sensor.setUp(23, 24, 1.5)
    try:
        while(True):
            sensor.sendData(sensor.readHumidity())
            time.sleep(1)
    except Exception as ex:
        traceback.print_exc()
    finally:
        GPIO.cleanup() #this ensures a clean exit


