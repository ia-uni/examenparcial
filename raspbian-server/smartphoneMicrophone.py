from urllib.request import urlopen
import numpy as np
import soundfile as sf
import io
import app
import time
import socket

socket.setdefaulttimeout(2.0)


class Microphone:
    def setUp(self, url):
        self.url = url

    def readData(self):
        try:
            audioPath=urlopen(self.url)
            audioData = audioPath.read(800)
            data , samplerate = sf.read(io.BytesIO(audioData))
            return data
        except Exception as ex:
            return None
    
    def sendData(self, data):
        app.send_sensor_data('microphone' , {'value' : data.tolist()})
        
if __name__ == '__main__':
    sensor = Microphone()
    sensor.setUp("http://192.168.2.101:8080/audio.wav")
    while (True):
        audio = sensor.readData()
        if (audio is not None):
            sensor.sendData(audio)
            time.sleep(0.1)
        





    
