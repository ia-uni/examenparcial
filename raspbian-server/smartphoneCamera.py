from urllib.request import urlopen
import numpy as np
from PIL import Image
import io
import app
import time
import socket
socket.setdefaulttimeout(2.0)


class Camera:
    def setUp(self, url):
        self.url = url

    def readData(self):
        try:
            imgPath=urlopen(self.url)
            image_data = imgPath.read()
            img = Image.open(io.BytesIO(image_data))
            return img
        except Exception as ex:
            return None
        

    def getHistogram(self , img):
        width, height = img.size
        redHistogram = []
        greenHistogram = []
        blueHistogram = []
        for i in range(256):
            redHistogram.append(0)
            greenHistogram.append(0)
            blueHistogram.append(0)
            
        for x in range(width):
            for y in range(height):
                colors = img.getpixel((x, y))
                redHistogram[colors[0]] += 1 
                greenHistogram[colors[1]] +=1
                blueHistogram[colors[2]] += 1
        return (redHistogram, greenHistogram, blueHistogram)
    
    def sendData(self, histogram):
        response = {
            "red" : histogram[0],
            "green" : histogram[1],
            "blue" : histogram[2]
        }
        app.send_sensor_data('camera' , response)
        
if __name__ == '__main__':
    sensor = Camera()
    sensor.setUp("http://192.168.2.101:8080/shot.jpg")
    while (True):
        frame = sensor.readData()
        if (frame is not None):
            histogram = sensor.getHistogram(frame)
            sensor.sendData(histogram)
            time.sleep(1)
        





    
