from urllib.request import urlopen
import numpy as np
from PIL import Image
import io
import app
import time
import socket
import json
socket.setdefaulttimeout(2.0)


class Accelerometer:
    def setUp(self, url):
        self.url = url

    def readData(self):
        try:
            accPath = urlopen(self.url)
            accData = accPath.read().decode('utf-8')
            accJson = json.loads(accData)
            acc = accJson['accel']['data'][0][1]
            ax = acc[0]
            ay = acc[1]
            az = acc[2]
            return ax , ay, az
        except Exception as ex:
            print("ex = " , ex)
            return None , None , None
    
    def sendData(self, ax, ay , az):
        response = {
            "ax" : ax,
            "ay" : ay,
            "az" : az
        }

        app.send_sensor_data('accelerometer' , response)
        
if __name__ == '__main__':
    sensor = Accelerometer()
    sensor.setUp("http://192.168.2.101:8080/sensors.json?sense=accel")
    while (True):
        st = time.time()
        ax, ay , az = sensor.readData()
        if (ax is not None):
            sensor.sendData(ax, ay , az)
            #time.sleep(0.1)
        





    
