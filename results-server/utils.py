from json import loads

def emit_sensor_data(io, sensor, request_json):
	io.emit(sensor, request_json)