var socket = io('/');

socket.on('connect', () => {
	console.log('Connected!');
});

var sensorCharts = {
	'cameraRed' : null,
	'cameraGreen' : null,
	'cameraBlue' : null,
	'microphone' : null,
	'accelerometer' : null,
	'humidity' : null,
	'distance' : null
};

var initialTime = {
	'camera' : null,
	'microphone' : null,
	'accelerometer' : null,
	'humidity' : null,
	'distance' : null
};

function createTitle(text){
	var myDiv = document.createElement('div');
	myDiv.style.width = '60%';
	var title = document.createElement("h1");
	title.innerHTML = text;
	title.style = "text-align:center;color:DodgerBlue";
	myDiv.appendChild(title);
	document.body.append(myDiv);
}

function addData(chart, label, data, i = 0, doUpdate = true) {
	if(label != null){
		chart.data.labels.push(label);
	}
	chart.data.datasets[i].data.push(data);
	if(doUpdate){
		chart.update();
	}
}

function addScatter(chart, xp, yp, doUpdate = true) {
    chart.data.datasets[0].data.push({x : xp, y : yp});
    if(doUpdate){
		chart.update();
	}
}

function updateData(chart , i , data){
	chart.data.datasets[0].data[i] = data;
}

function createChart(type , sensor , maxValue , YLabel, XLabel ,colorBack, colorBorder = 'rgba(0, 0, 0, 1)' , border = 1){
	var myDiv = document.createElement('div');
	myDiv.style.width = '60%';
	myDiv.style.height = '40%';
	var ctx = document.createElement('canvas');
	ctx.width = 400
	ctx.height = 120
	myDiv.appendChild(ctx);
	document.body.append(myDiv);
	ctx = ctx.getContext('2d');
	newChart = new Chart(ctx, {
		type: type,
		data: {
			labels: [],
			datasets: [{
				label: sensor,
				data: [],
				borderColor: colorBorder,
				backgroundColor: colorBack,
				borderWidth: border
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true,
						suggestedMax : maxValue
					},
					scaleLabel: {
						display: true,
						labelString : YLabel
					}
				}],
				xAxes: [{
					scaleLabel: {
						display: true,
						labelString : XLabel
					}
				}]
			}
		}
	});
	return newChart;
}

window.chartColors = {
      red: 'rgb(255, 99, 132)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
};
    
function createAccelerometerChart(){
	var myDiv = document.createElement('div');
	myDiv.style.width = '60%';
	myDiv.style.height = '40%';
	var ctx = document.createElement('canvas');
	ctx.width = 400
	ctx.height = 120
	myDiv.appendChild(ctx);
	document.body.append(myDiv);
	ctx = ctx.getContext('2d');
	newChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [],
			datasets: [{
				label: 'Aceleración X',
				data: [],
				borderColor: window.chartColors.red,
				backgroundColor: window.chartColors.red,
				fill : false
			},{
				label: 'Aceleración Y',
				data: [],
				borderColor: window.chartColors.blue,
				backgroundColor: window.chartColors.blue,
				fill : false
			}, {
				label: 'Aceleración Z',
				data: [],
				borderColor: window.chartColors.yellow,
				backgroundColor: window.chartColors.yellow,
				fill : false
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true,
					},
					scaleLabel: {
						display: true,
						labelString : 'Aceleración (m / s ^ 2)'
					}
				}],
				xAxes: [{
					scaleLabel: {
						display: true,
						labelString : 'Tiempo'
					}
				}]
			}
		}
	});
	return newChart;
}

socket.on('accelerometer', data => {
	if(sensorCharts['accelerometer'] == null){
		createTitle("Resultados Acelerómetro");
		sensorCharts['accelerometer'] = createAccelerometerChart();
		var curDate= new Date();
		initialTime['accelerometer'] = curDate.getTime();
	}
	var curDate= new Date();
	var curTime = (curDate.getTime() - initialTime['accelerometer']) / 1000;
	addData(sensorCharts['accelerometer'], curTime, parseFloat(data.ax), 0);
	addData(sensorCharts['accelerometer'], null, parseFloat(data.ay), 1);
	addData(sensorCharts['accelerometer'], null, parseFloat(data.az), 2);
})

var it = 0;
socket.on('microphone', data => {
	if(sensorCharts['microphone'] == null){
		createTitle("Resultados Micrófono");
		sensorCharts['microphone'] = createChart('scatter', 'Audio' , 0.3, 'Amplitud' , '' ,'rgba(0, 0, 0, 0)' , 'rgba(0,0,255, 1)' , 0.5);
	}
	sensorCharts['microphone'].reset();
	for(let x of data.value){
		addScatter(sensorCharts['microphone'], it, parseFloat(x) , false);
		it++;
	}
	sensorCharts['microphone'].update();
})

socket.on('camera', data => {
	if(sensorCharts['cameraRed'] == null){
		createTitle("Histograma (RGB) - Cámara Smartphone");
		var chartRed = createChart('bar', 'Red' , 1000 , '', 'Intensidad de color' ,'rgba(255, 0, 0, 1)' , 'rgba(255, 0, 0, 1)' );
		var chartGreen = createChart('bar', 'Green' , 1000 , '', 'Intensidad de color' ,'rgba(0, 255, 0, 1)' , 'rgba(0, 255, 0, 1)' );
		var chartBlue = createChart('bar', 'Blue' , 1000 , '', 'Intensidad de color' ,'rgba(0, 0, 255, 1)', 'rgba(0, 0, 255, 1)');
		sensorCharts['cameraRed'] = chartRed;
		sensorCharts['cameraGreen'] = chartGreen;
		sensorCharts['cameraBlue'] = chartBlue;
		for(var i = 0; i <= 255; i++){
			addData(sensorCharts['cameraRed'] , i , data.red[i], 0, false)
		}
		for(var i = 0; i <= 255; i++){
			addData(sensorCharts['cameraGreen'] , i , data.green[i] , 0 , false)
		}
		for(var i = 0; i <= 255; i++){
			addData(sensorCharts['cameraBlue'] , i , data.blue[i], 0 , false)
		}
		sensorCharts['cameraRed'].update();
		sensorCharts['cameraGreen'].update();
		sensorCharts['cameraBlue'].update();
	} else{
		for(var i = 0; i <= 255; i++){
			updateData(sensorCharts['cameraRed'] , i , data.red[i])
		}
		sensorCharts['cameraRed'].update();
		for(var i = 0; i <= 255; i++){
			updateData(sensorCharts['cameraGreen'] , i , data.green[i])
		}
		sensorCharts['cameraGreen'].update();
		for(var i = 0; i <= 255; i++){
			updateData(sensorCharts['cameraBlue'] , i , data.blue[i])
		}
		sensorCharts['cameraBlue'].update();
	}


})
socket.on('humidity', data => {
	if(sensorCharts['humidity'] == null){
		createTitle("Resultados Sensor de Humedad");
		sensorCharts['humidity'] = createChart('line', 'Humedad' , 100, 'Porcentaje (%)' , 'Tiempo' ,'rgba(75, 192, 192, 0.6)');
		var curDate= new Date();
		initialTime['humidity'] = curDate.getTime();
	}
	var curDate= new Date();
	var curTime = (curDate.getTime() - initialTime['humidity']) / 1000;
	addData(sensorCharts['humidity'], curTime, parseFloat(data.value));

})
socket.on('distance', data => {
	if(sensorCharts['distance'] == null){
		createTitle("Resultados Sensor de Distancia");
		sensorCharts['distance'] = createChart('line', 'Distancia' , 100, 'Distancia (cm)' , 'Tiempo' ,'rgba(255, 99, 132, 0.6)');
		var curDate= new Date();
		initialTime['distance'] = curDate.getTime();
	}
	var curDate= new Date();
	var curTime = (curDate.getTime() - initialTime['distance']) / 1000;
	addData(sensorCharts['distance'], curTime, parseFloat(data.value));
})


