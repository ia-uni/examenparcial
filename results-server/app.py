from flask import Flask, send_from_directory, request
from flask_socketio import SocketIO
from time import sleep
from os import getcwd as cwd
from utils import emit_sensor_data

app = Flask(__name__)
io = SocketIO(app)

counter = 0

@app.route('/<path:path>', methods=['GET'])
def static_proxy(path):
  return send_from_directory('./dist', path)

@app.route('/')
def root():
  return send_from_directory('./dist', 'index.html')

@app.route('/api/<sensor>', methods=['POST'])
def sensorInput(sensor=None):
	emit_sensor_data(io, sensor, request.json)
	return { 'success': True }

if __name__ == "__main__":
	io.run(app, host = '0.0.0.0', port=5002, debug=True)
